/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "celephais_worker_signals.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pthread.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxo/amxo.h>
#include <debug/sahtrace.h>

#include <celephais/celephais_defines.h>
#include "celephais.h"


typedef struct _sig_item {
    amxc_var_t data;
    celephais_cb_t func;
    amxd_object_t* obj;
    amxc_lqueue_it_t it;
} sig_item_t;

static celephais_worker_sig_ctrl_t worker_sig_ctrl;

celephais_worker_sig_ctrl_t* celephais_get_worker_sig_ctrl(void) 
{
    return &worker_sig_ctrl;
}

static sig_item_t* worker_sig_ctrl_dequeue(void) 
{
    celephais_worker_sig_ctrl_t* ctrtl = celephais_get_worker_sig_ctrl();
    amxc_lqueue_it_t* sig_it = amxc_lqueue_remove(&ctrtl->queue);
    sig_item_t* item = NULL;
    if(sig_it != NULL) 
    {
        item = amxc_llist_it_get_data(sig_it, sig_item_t, it);
    }

    return item;
}

static int read_worker_notification(int fd, sig_item_t** item) 
{
    ssize_t read_length = 0;
    char buffer[1];
    worker_sig_ctrl_lock();
    read_length = read(fd, buffer, 1);
    if(read_length < 1) 
    {
        goto exit;
    }
    // Retrieve amxc_var_t from the queue
    *item = worker_sig_ctrl_dequeue();

    when_null((*item), exit);
    amxc_llist_it_take(&(*item)->it);

exit:
    worker_sig_ctrl_unlock();
    return 0;
}

int worker_sig_ctrl_init(void) 
{
    int flags = 0;
    int retval = pipe(worker_sig_ctrl.pipe);
    if(retval != 0) 
    {
        goto exit;
    }

    flags = fcntl(worker_sig_ctrl.pipe[0], F_GETFL, 0);
    if(flags < 0) 
    {
        goto exit;
    }
    if(fcntl(worker_sig_ctrl.pipe[0], F_SETFL, flags | O_NONBLOCK) < 0) 
    {
        goto exit;
    }

    amxo_connection_add(celephais_get_parser(), worker_sig_ctrl_fd(), worker_notification_handler, NULL, AMXO_LISTEN, NULL);

    amxc_lqueue_init(&worker_sig_ctrl.queue);
    pthread_mutex_init(&worker_sig_ctrl.mutex, NULL);

    retval = 0;

exit:
    if(retval != 0) 
    {
        if(worker_sig_ctrl.pipe[0] != -1) 
        {
            close(worker_sig_ctrl.pipe[0]);
            close(worker_sig_ctrl.pipe[1]);
        }
        worker_sig_ctrl.pipe[0] = -1;
        worker_sig_ctrl.pipe[1] = -1;
    }
    return retval;
}

void worker_sig_ctrl_clean(void) 
{
    sig_item_t* item = NULL;
    char buffer[1];

    amxc_llist_for_each(it, (&worker_sig_ctrl.queue)) 
    {
        item = amxc_llist_it_get_data(it, sig_item_t, it);
        amxc_var_clean(&item->data);
        amxc_llist_it_take(&item->it);
        ssize_t s = read(worker_sig_ctrl.pipe[0], buffer, 1);
        if(!s) 
        {
            fprintf(stderr, "read error\n");
        }
    }

    amxo_connection_remove(celephais_get_parser(), worker_sig_ctrl_fd());
    if(worker_sig_ctrl.pipe[0] != -1) 
    {
        close(worker_sig_ctrl.pipe[0]);
    }
    if(worker_sig_ctrl.pipe[1] != -1) 
    {
        close(worker_sig_ctrl.pipe[1]);
    }

    pthread_mutex_destroy(&worker_sig_ctrl.mutex);
}

int worker_write_notification(amxd_object_t* obj, amxc_var_t* data, celephais_cb_t func) 
{
    int retval = -1;
    ssize_t write_length = 0;

    worker_sig_ctrl_lock();

    // Trigger event loop by writing a byte to the ctrl pipe
    write_length = write(worker_sig_ctrl.pipe[1], "I", 1);
    if(write_length != 1) 
    {
        goto exit;
    }

    celephais_worker_sig_ctrl_t* w = celephais_get_worker_sig_ctrl();
    sig_item_t* item = (sig_item_t*) calloc(1, sizeof(sig_item_t));

    amxc_var_init(&item->data);
    if(data != NULL) 
    {
        when_failed(amxc_var_copy(&item->data, data), exit);
    }
    item->obj = obj;
    item->func = func;
    amxc_lqueue_add(&w->queue, &item->it);

    retval = 0;
exit:
    worker_sig_ctrl_unlock();
    return retval;
}

void worker_notification_handler(int fd, UNUSED void* priv) 
{
    sig_item_t* item = NULL;
    if(read_worker_notification(fd, &item) == 0) 
    {
        if(item)
        {
            item->func(item->obj, &item->data);
            amxc_var_clean(&item->data);
        }
    }
}

int worker_sig_ctrl_fd(void) 
{
    return worker_sig_ctrl.pipe[0];
}

int worker_sig_ctrl_lock(void) 
{
    return pthread_mutex_lock(&worker_sig_ctrl.mutex);
}

int worker_sig_ctrl_unlock(void) 
{
    return pthread_mutex_unlock(&worker_sig_ctrl.mutex);
}