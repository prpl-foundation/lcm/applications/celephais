/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "celephais_dm.h"

#include <celephais/celephais_defines.h>
#include "celephais.h"
#include "celephais_worker.h"
#include "celephais_common.h"

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <string.h>
#include <debug/sahtrace.h>
#include <dirent.h>

//Necessary for extraction and JSON parsing and cleanup.
#include <archive.h>
#include <archive_entry.h>
#include <locale.h>
#include <linux/limits.h> 
#include <yajl/yajl_tree.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#define __USE_XOPEN_EXTENDED
#include <ftw.h>

#define ME "celephais_dm"

#define NOTIF_FAILED(obj, command_id, type, cmd, ...) SAH_TRACEZ_ERROR(ME, __VA_ARGS__); \
    celephais_notify_error(obj, command_id, type, cmd, __VA_ARGS__)

static amxc_var_t* amxc_var_add_new_key_cstring_from_object(amxc_var_t* var, const char* var_key, amxd_object_t* obj, const char* obj_key) 
{
    amxc_var_t* subvar = NULL;

    when_null(var, exit);
    subvar = amxc_var_add_new_key(var, var_key);
    when_null(subvar, exit);

    if(amxc_var_push(cstring_t, subvar, amxd_object_get_value(cstring_t, obj, obj_key, NULL)) != 0) {
        amxc_var_delete(&subvar);
    }

exit:
    return subvar;
}
static void add_objparam_to_htable(amxc_var_t* params, amxd_object_t* bundle, const char* key) {
    amxc_var_add_new_key_cstring_from_object(params, key, bundle, key);
}

static void fill_in_notification_bundle_data(amxc_var_t* params, amxd_object_t* bundle) 
{
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_NAME);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_DUID);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_URI);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_VERSION);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_VENDOR);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_DESCRIPTION);
    add_objparam_to_htable(params, bundle, CELEPHAIS_DM_BUNDLE_MARK_RM);
}

static amxc_var_t* celephais_notif_init_htable(amxd_object_t* obj, const char* command_id, amxc_var_t* notif) 
{
    amxc_var_t* params = NULL;

    amxc_var_set_type(notif, AMXC_VAR_ID_HTABLE);
    if(!string_is_empty(command_id)) {
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_COMMAND_ID, command_id);
    }

    params = amxc_var_add_key(amxc_htable_t, notif, CELEPHAIS_NOTIF_PARAMETERS, NULL);
    amxc_var_add_new_key_cstring_from_object(params, CELEPHAIS_NOTIF_CONTAINER_ID, obj, "Name");

    return params;
}

static void set_notif_error_type(amxc_var_t* notif, celephais_err_type_t type,
                                 const char* command_id, const char* command,
                                 const char* reason, va_list args) {

    amxc_var_set_type(notif, AMXC_VAR_ID_HTABLE);
    if(!string_is_empty(command_id)) {
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_COMMAND_ID, command_id);
    }
    amxc_var_add_key(uint64_t, notif, CELEPHAIS_NOTIF_ERROR_TYPE, type);
    switch(type) {
    case celephais_err_request_denied:
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_TYPESTR, "request denied");
        break;
    case celephais_err_invalid_arguments:
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_TYPESTR, "invalid arguments");
        break;
    case celephais_err_system_resources_exceeded:
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_TYPESTR, "system resources exceeded");
        break;
    default:
        amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_TYPESTR, "unknown");
    }
    if(!string_is_empty(command) || !string_is_empty(reason)) {
        if(!string_is_empty(command)) {
            amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_COMMAND, command);
        }
        if(!string_is_empty(reason)) {
            amxc_string_t r;
            amxc_string_init(&r, 0);
            amxc_string_vsetf(&r, reason, args);
            amxc_var_add_key(cstring_t, notif, CELEPHAIS_NOTIF_ERROR_REASON, amxc_string_get(&r, 0));
            amxc_string_clean(&r);
        }
    }
}

static void celephais_notify_error(amxd_object_t* obj, const char* command_id, celephais_err_type_t type, const char* command, const char* reason, ...)
{
    amxc_var_t notif;
    amxc_var_init(&notif);
    va_list args;

    if(!obj) {
        SAH_TRACEZ_ERROR(ME, "obj is null");
        goto exit;
    }

    va_start(args, reason);
    set_notif_error_type(&notif, type, command_id, command, reason, args);
    va_end(args);

    amxd_object_emit_signal(obj, CELEPHAIS_NOTIF_ERROR, &notif);
    SAH_TRACE_INFO("Bundle error fired with reason: [%s]", reason);
exit:
    amxc_var_clean(&notif);
}

//fire general bundle event
static void celephais_notify_bundle(amxd_object_t* bundle, const char* command_id, const char* event)
{
    amxc_var_t notif;
    amxc_var_t* params = NULL;
    amxc_var_init(&notif);

    if(bundle == NULL) {
        SAH_TRACEZ_ERROR(ME, "obj is null");
        goto exit;
    }
    
    params = celephais_notif_init_htable(bundle, command_id, &notif);
    fill_in_notification_bundle_data(params, bundle);
    amxd_object_emit_signal(bundle,event,&notif);
    SAH_TRACE_INFO("Bundle event fired: [%s]", event);
exit:
    amxc_var_clean(&notif);
}

static void celephais_notify_bundle_pulled(amxd_object_t* obj, const char* command_id)
{
    celephais_notify_bundle(obj, command_id, CELEPHAIS_NOTIF_BUNDLE_PULLED);
}

static void celephais_notif_bundle_remove_mark(amxd_object_t* obj, const char* command_id) {
    celephais_notify_bundle(obj, command_id, CELEPHAIS_NOTIF_BUNDLE_MARKED_RM);
}

static void celephais_notify_bundle_removed(amxd_object_t* obj, const char* command_id)
{
    celephais_notify_bundle(obj, command_id, CELEPHAIS_NOTIF_BUNDLE_REMOVED);
}

//Duplicate function from cthulhu. These can be extracted to common helper library.
//Same goes for cthulhu helpers like mkdir etc.
// code used from https://github.com/libarchive/libarchive/blob/master/examples/untar.c
static la_ssize_t copy_data(struct archive* ar, struct archive* aw) {
    la_ssize_t r;
    const void* buff;
    size_t size;
#if ARCHIVE_VERSION_NUMBER >= 3000000
    int64_t offset;
#else
    off_t offset;
#endif

    while(true) {
        r = archive_read_data_block(ar, &buff, &size, &offset);
        if(r == ARCHIVE_EOF) {
            return (ARCHIVE_OK);
        }
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "archive_read_data_block() error: %s",
                             archive_error_string(ar));
        return (r);
        }
        r = archive_write_data_block(aw, buff, size, offset);
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "archive_write_data_block() error: %s",
                             archive_error_string(aw));
            return (r);
        }
    }
}

//Duplicate function from cthulhu. These can be extracted to common helper library.
//Same goes for cthulhu helpers like mkdir etc.
// extract code borrowed from https://github.com/libarchive/libarchive/blob/master/examples/untar.c
static int extract(const char* filename, const char* out_location) {
    struct archive* a = archive_read_new();
    struct archive* ext = archive_write_disk_new();
    struct archive_entry* entry;
    int r = ARCHIVE_FAILED;
    char out_path[PATH_MAX];
    // flag definitions:
    // https://www.freebsd.org/cgi/man.cgi?query=archive_write_disk&sektion=3&format=html
    int flags = ARCHIVE_EXTRACT_TIME |
        ARCHIVE_EXTRACT_PERM |
        ARCHIVE_EXTRACT_ACL |
        ARCHIVE_EXTRACT_FFLAGS |
        ARCHIVE_EXTRACT_SECURE_NODOTDOT |
        ARCHIVE_EXTRACT_SECURE_SYMLINKS |
        ARCHIVE_EXTRACT_XATTR;

    archive_write_disk_set_options(ext, flags);
    archive_read_support_filter_gzip(a);
    archive_read_support_format_tar(a);

    if((filename != NULL) && (strcmp(filename, "-") == 0)) {
        filename = NULL;
    }
    r = archive_read_open_filename(a, filename, 10240);
    if(r != 0) {
        SAH_TRACEZ_ERROR(ME, "archive_read_open_filename() error: %s", archive_error_string(a));
        goto exit;
    }
    while(true) {
        r = archive_read_next_header(a, &entry);
        if(r == ARCHIVE_EOF) {
            break;
        }
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_ERROR(ME, "archive_read_next_header() error: %s", archive_error_string(a));
            goto exit;
        }
        snprintf(out_path, PATH_MAX, "%s/%s", out_location, archive_entry_pathname(entry));
        archive_entry_set_pathname(entry, out_path);
        /* A hardlink must point to a real file, so set its output directory too. */
        const char* original_hardlink = archive_entry_hardlink(entry);
        if(original_hardlink) {
            char new_hardlink[PATH_MAX];
            sprintf(new_hardlink, "%s/%s", out_location, original_hardlink);
            archive_entry_set_hardlink(entry, new_hardlink);
        }
        r = archive_write_header(ext, entry);
        if(r != ARCHIVE_OK) {
            SAH_TRACEZ_WARNING(ME,
                               "archive_write_header() file: %s error: %s",
                               out_path, archive_error_string(ext));
        } else {
            copy_data(a, ext);
            r = archive_write_finish_entry(ext);
            if(r != ARCHIVE_OK) {
                SAH_TRACEZ_ERROR(ME,
                                 "archive_write_finish_entry() fle: %s error: %s",
                                 out_path, archive_error_string(ext));
            }
        }
    }
    r = ARCHIVE_OK;

exit:
    if(a) {
        archive_read_close(a);
        archive_read_free(a);
    }
    if(ext) {
        archive_write_close(ext);
        archive_write_free(ext);
    }
    return r;
}

static amxd_status_t create_bundle_dm(const char* duid, const char* uri, const char* version, const char* name, const char* vendor, const char* description, bool markforremoval)
{
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* bundles = amxd_dm_findf(celephais_get_dm(), CELEPHAIS_DM_BUNDLES);
    amxd_trans_t trans;

    if(duid == NULL) {
        SAH_TRACEZ_ERROR(ME, "No DUID provided");
        status = amxd_status_invalid_arg;
        goto exit;
    }

    if(version == NULL) {
        SAH_TRACEZ_ERROR(ME, "No version provided");
        status = amxd_status_invalid_arg;
        goto exit;
    }

    amxd_trans_init(&trans);

    amxd_trans_select_object(&trans, bundles);
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_DUID, duid);
    amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_VERSION, version);
    if(uri) {
        amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_URI, uri);
    } else {
        SAH_TRACEZ_ERROR(ME, "No URI provided");
    }
    if(name) {
        amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_NAME, name);
    }
    if(vendor) {
        amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_VENDOR, vendor);
    } else {
        SAH_TRACEZ_ERROR(ME, "No vendor provided");
    }
    if(description) {
        amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_DESCRIPTION, description);
    }

    amxd_trans_set_value(bool, &trans, CELEPHAIS_DM_BUNDLE_MARK_RM, markforremoval);
    amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_STATUS, CELEPHAIS_STATUS_DOWNLOADED);
    amxd_trans_set_value(uint32_t, &trans, CELEPHAIS_DM_BUNDLE_ERROR_CODE, 0);

    status = amxd_trans_apply(&trans, celephais_get_dm());
    if(status != amxd_status_ok) {
        SAH_TRACEZ_NOTICE(ME, "err[%s]: Transaction failed in func . Status: %d", __func__, status);
    }
    amxd_trans_clean(&trans);
exit:
    return status;
}

int helper_rmdir(const char* path, UNUSED const struct stat* s, UNUSED int flag, UNUSED struct FTW* f) {
    if(string_is_empty(path)) {
        return ENOENT;
    }

    int status = 0;
    status = remove(path);
    if (status != 0) {
        SAH_TRACEZ_ERROR(ME, "Couldn't remove %s, error %d", path, status);
    }
    return status;
}

int remove_leftovers(const char* dir)
{
    const uint8_t max_used_fds = 16;
    if(nftw(dir, helper_rmdir, max_used_fds, FTW_DEPTH|FTW_MOUNT|FTW_PHYS)){
        return errno;
    }
    return 0;
}

static int extract_bundle_info(const char* bundle_location, const char* duid,
        const char** keys,
        const size_t keys_count,
        const char** json_path,
        const size_t path_count,
        amxc_llist_t* params)
{
    amxc_string_t* bundle_tar = NULL;
    amxc_string_t* bundle_path = NULL;
    amxc_string_t* bundle_config = NULL;
    amxc_string_t* bundle_tmp_location = NULL;
    amxc_string_new(&bundle_tar, 0);
    amxc_string_setf(bundle_tar, "%s.tar", duid);
    amxc_string_new(&bundle_tmp_location, 0);
    amxc_string_setf(bundle_tmp_location, "%s/%s", bundle_location, duid);
    amxc_string_new(&bundle_path, 0);
    amxc_string_setf(bundle_path, "%s/%s", bundle_location, bundle_tar->buffer);
    amxc_string_new(&bundle_config, 0);
    amxc_string_setf(bundle_config, "%s/%s", bundle_tmp_location->buffer, "config.json");

    int ret = 0;
    struct stat st;
    if (stat(bundle_tmp_location->buffer, &st) == -1) {
        ret = mkdir(bundle_tmp_location->buffer, 0766);
        if (ret == -1 && errno != EEXIST){
            SAH_TRACEZ_ERROR(ME, "[%s]: Cannot create tmpdir for parsing of config.json - errno: %s", __func__, strerror(errno));
            ret = errno;
            goto exit;
        }
    }
    extract(bundle_path->buffer, bundle_tmp_location->buffer);

    memset(&st, 0, sizeof(st));
    if (stat(bundle_config->buffer, &st)) {
        SAH_TRACEZ_ERROR(ME, "[%s]: Cannot stat config.json - errno: %s", __func__, strerror(errno));
        ret = errno;
        goto exit;
    }
    const size_t size = (size_t)st.st_size;

    char errbuf[256];
    char* json_buf = calloc(sizeof(char), size+1);
    if (!json_buf) {
        SAH_TRACEZ_ERROR(ME, "[%s]: Not enough memory on heap - aborting", __func__);
        ret = ENOMEM;
        goto exit;
    }

    FILE* json_fd = fopen(bundle_config->buffer, "r");
    if (!json_fd) {
        SAH_TRACEZ_ERROR(ME, "Error while opening json file under %s, %s", bundle_path->buffer, strerror(errno));
        ret = errno;
        goto free_json_buf;
    }

    fread((void *) json_buf, size , 1, json_fd);
    if (ferror(json_fd)) {
        SAH_TRACEZ_ERROR(ME, "Error while reading config.json into memory %s, %s", bundle_path->buffer, strerror(errno));
        ret = -1;
        goto close_json_fd;
    }

    yajl_val node;
    node = yajl_tree_parse((const char *)json_buf, errbuf, sizeof(errbuf));
    if (!node) {
        SAH_TRACEZ_ERROR(ME, "Error while parsing config.json - errdata: %s", errbuf);
        ret = -1;
        goto close_json_fd;
    }

    //find first NULL occurrence
    uint8_t path_idx = 0;
    for (; path_idx < path_count && json_path[path_idx] != NULL; ++path_idx) {}

    for (size_t idx = 0; idx < keys_count; ++idx) {
        json_path[path_idx] = keys[idx];
        amxc_llist_it_t* it = amxc_llist_get_at(params, (uint32_t)idx);
        amxc_var_t* var = amxc_var_from_llist_it(it);
        yajl_val v = yajl_tree_get(node, json_path, yajl_t_string);
        if (!v) {
            uint8_t err_idx = 0;
            while (err_idx < path_idx) {
                snprintf(errbuf, sizeof(errbuf), "%s/", json_path[err_idx++]);
            }
            SAH_TRACEZ_ERROR(ME, "No such node: %s", errbuf);
            amxc_var_set(cstring_t, var, "Unknown");
            continue;
        }
        amxc_var_set(cstring_t, var, YAJL_GET_STRING(v));
    }

    yajl_tree_free(node);
close_json_fd:
    fclose(json_fd);
free_json_buf:
    free(json_buf);
exit:
    remove_leftovers(bundle_tmp_location->buffer);
    amxc_string_delete(&bundle_path);
    amxc_string_delete(&bundle_tar);
    amxc_string_delete(&bundle_config);
    amxc_string_delete(&bundle_tmp_location);
    return ret;
}

static amxd_status_t sync_bundle_dm(const char* bundle_location, const char* filename, size_t l)
{   //incomplete. This will need to retrieve the rest of the values also eventually. its only DUID for now
    amxd_status_t res = amxd_status_ok;
    amxc_string_t duid;
    amxc_string_init(&duid,0);
    amxc_string_push_buffer(&duid,strdup(filename),l+1);
    amxc_string_shrink(&duid,4);

    amxc_llist_t params;
    amxc_llist_init(&params);
    static const char* keys_ann[] = { "org.opencontainers.image.url",
           "org.opencontainers.image.version",
           "org.opencontainers.image.vendor",
           "org.opencontainers.artifact.description"
    };
    const char *values[] = {NULL, NULL, NULL, NULL};
    const char* json_ann_path[] = {"annotations", NULL, NULL}; //changed from const char*
    const size_t path_count = sizeof(json_ann_path)/sizeof(char *); 
    const size_t keys_count = sizeof(keys_ann)/sizeof(char *); 

    for (size_t idx = 0; idx < keys_count; ++idx) {
        amxc_var_t *var;
        amxc_var_new(&var);
        amxc_var_set_type(var, AMXC_VAR_ID_CSTRING);
        amxc_llist_append(&params, &var->lit);
    }

    const int ret = extract_bundle_info(bundle_location, duid.buffer, keys_ann, keys_count, json_ann_path, path_count, &params);
    if (ret < 0) {
        res = create_bundle_dm(duid.buffer, "Unknown", "Unknown", duid.buffer, NULL, NULL, false);
        SAH_TRACEZ_ERROR(ME, "Failed to extract metadata from bundle");
    } else {
        int val_idx = 0;
        amxc_llist_iterate(it, &params) {
            values[val_idx] = amxc_var_constcast(cstring_t,
                amxc_llist_it_get_data(it, amxc_var_t, lit));
        ++val_idx;
    }
        res = create_bundle_dm(duid.buffer, values[0], values[1], duid.buffer, values[2], values[3], false);
    }

    amxc_llist_clean(&params, variant_list_it_free);
    amxc_string_clean(&duid);
    return res;
}

amxd_status_t celephais_sync_bundles(void)
{
    amxd_status_t res = amxd_status_ok;
    amxd_object_t* celephais = amxd_dm_get_object(celephais_get_dm(), CELEPHAIS_DM);

    char* bundle_location = amxd_object_get_value(cstring_t, celephais, CELEPHAIS_BUNDLE_LOCATION, NULL);
    struct dirent* de;
    DIR* dr = opendir(bundle_location);
    if(dr != NULL)
    {
        while((de = readdir(dr)) != NULL)
        {  
            size_t l = strlen(de->d_name);
            if(l < 5)
            {   // not long enough to contain '.tar' with atleast one char for name
                continue;
            }

            if(strcmp(".tar", &de->d_name[l-4]) == 0)
            {   //evil substring
                res = sync_bundle_dm(bundle_location, de->d_name,l);
            }

            if(res != amxd_status_ok) 
            {
                break;
            }
        }
        closedir(dr);
    }
    else
    {
        res = amxd_status_file_not_found;
    }

    return res;
}

void celephais_pull_cb(UNUSED amxd_object_t* obj, amxc_var_t* data)
{   //datamodel update, create dm instance, fire pulled event
    if(data==NULL)
    {
        //no data with which to update the datamodel. skip cb
        SAH_TRACEZ_NOTICE(ME, "No data to update the model with");
        return;
    }

    const char* uri = GET_CHAR(data, CELEPHAIS_CMD_PULL_URI);
    const char* duid = GET_CHAR(data, CELEPHAIS_CMD_PULL_DUID);
    UNUSED const char* command_id = GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID);
    
    amxd_object_t* celephais = amxd_dm_get_object(celephais_get_dm(), CELEPHAIS_DM);
    if (!celephais) {
        SAH_TRACEZ_INFO(ME, "CELEPHAIS_DM not returned");
    }
    const char* bundle_location = amxd_object_get_value(cstring_t, celephais, CELEPHAIS_BUNDLE_LOCATION, NULL);
    static const char* keys_ann[] = {"org.opencontainers.image.version",
           "org.opencontainers.image.vendor",
           "org.opencontainers.artifact.description"
    };
    const char *values[] = {NULL, NULL, NULL};
    const char* json_ann_path[] = {"annotations", NULL, NULL}; //changed from const char*
    const size_t path_count = sizeof(json_ann_path)/sizeof(char *); 
    const size_t keys_count = sizeof(keys_ann)/sizeof(char *); 
    
    amxc_llist_t params;
    amxc_llist_init(&params);
    for (size_t idx = 0; idx < keys_count; ++idx) {
        amxc_var_t *var;
        amxc_var_new(&var);
        amxc_var_set_type(var, AMXC_VAR_ID_CSTRING);
        amxc_llist_append(&params, &var->lit);
    }

    amxd_status_t res = amxd_status_ok;
    const int ret = extract_bundle_info(bundle_location, duid, keys_ann, keys_count, json_ann_path, path_count, &params);
    if (ret < 0) {
        res = create_bundle_dm(duid, uri, "Unknown", duid, NULL, NULL, false);
        SAH_TRACEZ_ERROR(ME, "Failed to extract metadata from bundle");
    } else {
        int val_idx = 0;
        amxc_llist_iterate(it, &params) {
            values[val_idx] = amxc_var_constcast(cstring_t,
                amxc_llist_it_get_data(it, amxc_var_t, lit));
            ++val_idx;
        }
        res = create_bundle_dm(duid, uri, values[0], duid, values[1], values[2], false);
    }

    amxc_llist_clean(&params, variant_list_it_free);
    //fire event
    amxd_object_t* bundle;
    switch(res)
    {
    case amxd_status_ok:
        bundle = amxd_dm_findf(celephais_get_dm(), CELEPHAIS_DM_BUNDLES ".[" CELEPHAIS_DM_BUNDLE_DUID "== '%s'].",duid);
        if(bundle)
        {
            celephais_notify_bundle_pulled(bundle, command_id);
        }
        else
        {
            SAH_TRACEZ_ERROR(ME,"can't find new bundle %s",duid);
        }
        break;
    case amxd_status_invalid_arg:
        NOTIF_FAILED(obj, command_id, celephais_err_invalid_arguments, CELEPHAIS_CMD_PULL, " invalid arguments");
        break;
    default:
        NOTIF_FAILED(obj, command_id, celephais_err_request_denied, CELEPHAIS_CMD_PULL, " unknown error: %d", res);
        break;
    }
}

static void update_bundle_uri(amxd_object_t* inst, const char* uri)
{
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, inst);
    amxd_trans_set_value(cstring_t, &trans, CELEPHAIS_DM_BUNDLE_URI, uri);

    amxd_trans_apply(&trans, celephais_get_dm());
    amxd_trans_clean(&trans);
}

void celephais_update_cb(UNUSED amxd_object_t* obj, amxc_var_t* data)
{
    if(data==NULL)
    {
        return;
    }

    const char* uri = GET_CHAR(data, CELEPHAIS_CMD_UPDATE_URI);
    const char* duid = GET_CHAR(data, CELEPHAIS_CMD_UPDATE_DUID);
    const char* command_id = GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID);
    amxd_object_t* bundle = amxd_dm_findf(celephais_get_dm(), CELEPHAIS_DM_BUNDLES ".[" CELEPHAIS_DM_BUNDLE_DUID " == \"%s\"].", duid);
    if (bundle == NULL) {
        NOTIF_FAILED(obj,
                     command_id,
                     celephais_err_request_denied,
                     CELEPHAIS_CMD_UPDATE,
                     "Could't find bundle with " CELEPHAIS_DM_BUNDLES " (%s)",
                     duid);
    }
    else
    {
        update_bundle_uri(bundle,uri);
        celephais_notify_bundle_pulled(bundle, command_id);
        //notify bundle pulled here, the adapter will use the command id to invoke update rather than create
    }
}

static void update_mark_for_removal(amxd_object_t* instance) {
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, instance);
    amxd_trans_set_value(bool, &trans, CELEPHAIS_DM_BUNDLE_MARK_RM, true);

    amxd_trans_apply(&trans, celephais_get_dm());
    amxd_trans_clean(&trans);
}

void celephais_remove_cb(amxd_object_t* obj, amxc_var_t* data)
{
    if ((obj == NULL) || (data == NULL))
    {
        SAH_TRACEZ_WARNING(ME, "Invalid arguments to %s", __func__);
        return;
    }

    const char* command_id = GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID);
    const char* duid = GET_CHAR(data, CELEPHAIS_CMD_REMOVE_DUID);
    const char* version = GET_CHAR(data, CELEPHAIS_CMD_REMOVE_VERSION);
    amxd_object_t* bundle = amxd_dm_findf(celephais_get_dm(), CELEPHAIS_DM_BUNDLES ".[" CELEPHAIS_DM_BUNDLE_DUID " == \"%s\" && " CELEPHAIS_DM_BUNDLE_VERSION " == '%s'].", duid, version);
    if (bundle == NULL) {
        NOTIF_FAILED(obj,
                     command_id,
                     celephais_err_request_denied,
                     CELEPHAIS_CMD_REMOVE,
                     "Could't find bundle with " CELEPHAIS_DM_BUNDLES " (%s) - " CELEPHAIS_DM_BUNDLE_VERSION " (%s)",
                     duid,
                     version);
    } else {
        SAH_TRACEZ_INFO(ME, "Mark bundle for removal in DM");
        update_mark_for_removal(bundle);
        celephais_notif_bundle_remove_mark(bundle, command_id);
    }

}

void celephais_bundle_removed_cb(amxd_object_t* obj, amxc_var_t* data) {
    if ((obj == NULL) || (data == NULL)) {
        SAH_TRACEZ_WARNING(ME, "Invalid arguments to %s", __func__);
        return;
    }
    amxd_object_for_each(instance, it, obj) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        bool removed = amxd_object_get_value(bool, instance, CELEPHAIS_DM_BUNDLE_MARK_RM, NULL);
        if(removed) {
            celephais_notify_bundle_removed(instance, GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID));
            amxd_object_delete(&instance); // Remove instance from the data model
        }
    }
}

//callback for emitting error events from workers
void celephais_cmd_failed(amxd_object_t* obj, amxc_var_t* data)
{
    if(!data) 
    {
        SAH_TRACEZ_WARNING(ME, "No data");
    }

    NOTIF_FAILED(obj, GET_CHAR(data, CELEPHAIS_NOTIF_COMMAND_ID),
                 GET_INT32(data, CELEPHAIS_NOTIF_ERROR_TYPE),
                 GET_CHAR(data, CELEPHAIS_COMMAND),
                 "%s", GET_CHAR(data, CELEPHAIS_NOTIF_ERROR_REASON));
}
