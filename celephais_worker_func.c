/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "celephais_worker_func.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>
#include <debug/sahtrace.h>

#include <celephais/celephais_defines.h>
#include <celephais/celephais_curl.h>
#include <celephais/celephais_utils.h>
#include <celephais/celephais_remove.h>
#include "celephais.h"
#include "celephais_worker_signals.h"
#include "celephais_dm.h"

#define ME "celephais_worker"

#define NOTIF_WORKER_FAILED(obj, command_id, type, cmd, ...) SAH_TRACEZ_INFO(ME,"Worker failed!"); \
    celephais_worker_emit_error(obj, command_id, type, cmd, __VA_ARGS__)

static int celephais_worker_emit_error(amxd_object_t* obj, const char* command_id, celephais_err_type_t type, const char* cmd, const char* reason, ...)
{
    int res = -1;
    va_list args;
    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data, CELEPHAIS_COMMAND, cmd);
    amxc_var_add_key(cstring_t, &data, CELEPHAIS_NOTIF_COMMAND_ID, command_id);
    amxc_var_add_key(int32_t, &data, CELEPHAIS_NOTIF_ERROR_TYPE, type);
    va_start(args, reason);
    if(!string_is_empty(reason)) {
        amxc_string_t r;
        amxc_string_init(&r, 0);
        amxc_string_vsetf(&r, reason, args);
        amxc_var_add_key(cstring_t, &data, CELEPHAIS_NOTIF_ERROR_REASON, amxc_string_get(&r, 0));
        amxc_string_clean(&r);
    }
    va_end(args);
    worker_write_notification(obj, &data, celephais_cmd_failed);

    amxc_var_clean(&data);
    return res;
}

celephais_err_type_t celephais_do_pull(amxd_object_t* obj, amxc_var_t* args)
{
    celephais_err_type_t res = celephais_no_error;

    amxd_object_t* celephais = amxd_dm_get_object(celephais_get_dm(), CELEPHAIS_DM);
    if (!celephais) {
        SAH_TRACEZ_INFO(ME, "CELEPHAIS_DM not returned");
    }
    char* bundle_location = amxd_object_get_value(cstring_t, celephais, CELEPHAIS_BUNDLE_LOCATION, NULL);
    char* dest_path = NULL;
    amxc_string_t uri_str;
    curl_off_t size = -1;

    const char* uri = GET_CHAR(args,CELEPHAIS_CMD_PULL_URI);
    const char* duid = GET_CHAR(args,CELEPHAIS_CMD_PULL_DUID);
    const char* username = GET_CHAR(args, CELEPHAIS_CMD_PULL_USERNAME);
    const char* password = GET_CHAR(args, CELEPHAIS_CMD_PULL_PASSWORD);
    UNUSED const char* command_id = GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID);

    asprintf(&dest_path, "%s/%s.tar", bundle_location, duid);

    amxc_string_init(&uri_str,0);
    amxc_string_push_buffer(&uri_str, strdup(uri), strlen(uri)+1);

    amxc_string_t* username_str = NULL;
    amxc_string_t* password_str = NULL;
    celephais_auth_type_t auth = NO_AUTHENTICATION;
    if(username && password)
    {   //use basic auth
        auth = BASIC_AUTHENTICATION;
        amxc_string_new(&username_str,0);
        amxc_string_new(&username_str,0); 
        amxc_string_push_buffer(username_str,strdup(username),strlen(username)+1);
        amxc_string_push_buffer(password_str,strdup(password),strlen(password)+1);
        SAH_TRACE_INFO("using creds");
    }
    else if(username != password)
    {   //username without password or password without username
        SAH_TRACEZ_INFO(ME, "incomplete creds");
        res = celephais_err_invalid_arguments;
        goto exit;
    }

    //No way to use bearer auth yet. But the curl function can handle it.
    celephais_curl_parameters_t bundle_parameters = {
        .username = username_str,
        .password = password_str,
        .token = NULL,
        .type = FILE_TYPE_TAR,
        .auth_type = auth
    };
    
    celephais_status_t curlrc;
    if(!file_exists(dest_path))
    {
        FILE* fd = fopen(dest_path,"w");
        curlrc = celephais_curl_download_content(&bundle_parameters, &uri_str, (void*) fd, &size, false);
        fclose(fd);
        if(curlrc)
        {   //clean up stub file if the download fails
            remove(dest_path);
        }
    }
    else
    {
        SAH_TRACEZ_INFO(ME, "File already exists: %s", dest_path);
        curlrc=CELEPHAIS_BUNDLE_ALREADY_EXISTS;
    }

    switch(curlrc)
    {
        case CELEPHAIS_NO_ERROR:
        case CELEPHAIS_BUNDLE_ALREADY_EXISTS:
            break;
        case CELEPHAIS_ERROR_INVALID_ARGUMENTS:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_invalid_arguments,CELEPHAIS_CMD_PULL, " celephais_curl(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_invalid_arguments;
            goto exit;
        case CELEPHAIS_ERROR_DISK_SPACE:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_system_resources_exceeded,CELEPHAIS_CMD_PULL, " celephais_curl(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_system_resources_exceeded;
            goto exit;
        case CELEPHAIS_ERROR_SIZE_CHECK:
        case CELEPHAIS_ERROR_INVALID_CONTENT:
        case CELEPHAIS_ERROR_INVALID_SIGNATURE:
        case CELEPHAIS_ERROR_CURL_OPERATION_TIMEDOUT:
        case CELEPHAIS_ERROR_CURL_COULDNT_RESOLVE_HOST:
        case CELEPHAIS_ERROR_DOWNLOAD_FAILED:
        case CELEPHAIS_ERROR_UNKNOWN_ERROR:
        default:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_request_denied, CELEPHAIS_CMD_PULL, " celephais_curl(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_request_denied;
            goto exit;
    }

    if(curlrc==CELEPHAIS_NO_ERROR)
    {
            worker_write_notification(obj, args, celephais_pull_cb);
    }
    
exit:
    if(auth==BASIC_AUTHENTICATION)
    {
        amxc_string_delete(&username_str);
        amxc_string_delete(&password_str);
    }
    amxc_string_clean(&uri_str);
    free(bundle_location);
    free(dest_path);
    return res;
}

celephais_err_type_t celephais_do_update(amxd_object_t* obj, amxc_var_t* args)
{
    celephais_err_type_t res = celephais_no_error;

    amxd_object_t* celephais = amxd_dm_get_object(celephais_get_dm(), CELEPHAIS_DM);
    char* bundle_location = amxd_object_get_value(cstring_t, celephais, CELEPHAIS_BUNDLE_LOCATION, NULL);
    char* dest_path = NULL;
    char* tmp_dest_path = NULL;
    amxc_string_t uri_str;
    curl_off_t size = -1;

    const char* uri = GET_CHAR(args,CELEPHAIS_CMD_PULL_URI);
    const char* duid = GET_CHAR(args,CELEPHAIS_CMD_PULL_DUID);
    const char* username = GET_CHAR(args, CELEPHAIS_CMD_PULL_USERNAME);
    const char* password = GET_CHAR(args, CELEPHAIS_CMD_PULL_PASSWORD);
    UNUSED const char* command_id = GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID);

    asprintf(&dest_path,"%s/%s.tar", bundle_location, duid);
    asprintf(&tmp_dest_path, "%s/%s-tmp.tar", bundle_location, duid);

    amxc_string_init(&uri_str,0);
    amxc_string_push_buffer(&uri_str, strdup(uri), strlen(uri)+1);

    amxc_string_t* username_str = NULL;
    amxc_string_t* password_str = NULL;
    celephais_auth_type_t auth = NO_AUTHENTICATION;
    if(username && password)
    {   //use basic auth
        auth = BASIC_AUTHENTICATION;
        amxc_string_new(&username_str,0);
        amxc_string_new(&username_str,0); 
        amxc_string_push_buffer(username_str,strdup(username),strlen(username)+1);
        amxc_string_push_buffer(password_str,strdup(password),strlen(password)+1);
        SAH_TRACE_INFO("using creds");
    }
    else if(username != password)
    {   //username without password or password without username
        SAH_TRACEZ_INFO(ME, "incomplete creds");
        res = celephais_err_invalid_arguments;
        goto exit;
    }

    celephais_curl_parameters_t bundle_parameters = {
        .username = username_str,
        .password = password_str,
        .token = NULL,
        .type = FILE_TYPE_TAR,
        .auth_type = auth
    };
    /*
        check that the file exists. if not, exit. If it does exist, delete and replace? or just overwrite into the same fd?
        success cb will find the datamodel entry and update the uri, fail as normal.
        Do not overwrite in case of failure!!
    */
    if(!file_exists(dest_path))
    {
        NOTIF_WORKER_FAILED(obj, command_id, celephais_err_invalid_arguments, CELEPHAIS_CMD_UPDATE, "celephais_update[%s] failed!, no such bundle exists to update", dest_path);
        res = celephais_err_invalid_arguments;
        goto exit;
    }

    //Download to temp path, only replace if the pull succeeds
    celephais_status_t curlrc;
    FILE* fd = fopen(tmp_dest_path, "w");
    curlrc = celephais_curl_download_content(&bundle_parameters, &uri_str, (void*) fd, &size, false);
    fclose(fd);
    if(curlrc)
    {
        remove(tmp_dest_path);
    }

    switch(curlrc)
    {
        case CELEPHAIS_NO_ERROR:
        case CELEPHAIS_BUNDLE_ALREADY_EXISTS:
            break;
        case CELEPHAIS_ERROR_INVALID_ARGUMENTS:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_invalid_arguments, CELEPHAIS_CMD_UPDATE, " celephais_update(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_invalid_arguments;
            goto exit;
        case CELEPHAIS_ERROR_DISK_SPACE:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_system_resources_exceeded, CELEPHAIS_CMD_UPDATE, " celephais_update(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_system_resources_exceeded;
            goto exit;
        case CELEPHAIS_ERROR_SIZE_CHECK:
        case CELEPHAIS_ERROR_INVALID_CONTENT:
        case CELEPHAIS_ERROR_INVALID_SIGNATURE:
        case CELEPHAIS_ERROR_CURL_OPERATION_TIMEDOUT:
        case CELEPHAIS_ERROR_CURL_COULDNT_RESOLVE_HOST:
        case CELEPHAIS_ERROR_DOWNLOAD_FAILED:
        case CELEPHAIS_ERROR_UNKNOWN_ERROR:
        default:
            NOTIF_WORKER_FAILED(obj, command_id, celephais_err_request_denied, CELEPHAIS_CMD_UPDATE, " celephais_update(%s) failed ! [%d]", uri, curlrc);
            res = celephais_err_request_denied;
            goto exit;
    }

    //curl is successful, replace the bundle
    remove(dest_path);
    rename(tmp_dest_path, dest_path);

    worker_write_notification(obj, args, celephais_update_cb);
exit:
    if(auth==BASIC_AUTHENTICATION)
    {
        amxc_string_delete(&username_str);
        amxc_string_delete(&password_str);
    }
    amxc_string_clean(&uri_str);
    free(bundle_location);
    free(dest_path);
    free(tmp_dest_path);
    return res;
}

celephais_err_type_t celephais_do_remove(amxd_object_t* obj, amxc_var_t* args)
{
    SAH_TRACEZ_INFO(ME, "%s entered", __func__);

    celephais_err_type_t rc = celephais_no_error;
    amxd_dm_t* dm = celephais_get_dm();
    amxd_object_t* bundle = NULL;
    const char* duid = GET_CHAR(args, CELEPHAIS_CMD_REMOVE_DUID);
    const char* version = GET_CHAR(args, CELEPHAIS_CMD_REMOVE_VERSION);
    UNUSED const char* command_id = GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID);

    if (string_is_empty(duid)) {
        NOTIF_WORKER_FAILED(obj, command_id, celephais_err_invalid_arguments, CELEPHAIS_CMD_REMOVE, CELEPHAIS_CMD_REMOVE_DUID " is empty");
        rc = celephais_err_invalid_arguments;
        goto exit;
    }
    if (string_is_empty(version)) {
        NOTIF_WORKER_FAILED(obj, command_id, celephais_err_invalid_arguments, CELEPHAIS_CMD_REMOVE, CELEPHAIS_CMD_REMOVE_VERSION " is empty");
        rc = celephais_err_invalid_arguments;
        goto exit;
    }

    bundle = amxd_dm_findf(dm, CELEPHAIS_DM_BUNDLES ".[" CELEPHAIS_DM_BUNDLE_DUID " == \"%s\" && " CELEPHAIS_DM_BUNDLE_VERSION " == '%s'].", duid, version);
    if(bundle == NULL) {
        NOTIF_WORKER_FAILED(obj,
                            command_id,
                            celephais_err_request_denied,
                            CELEPHAIS_CMD_REMOVE,
                            "Could't find bundle with " CELEPHAIS_DM_BUNDLE_DUID " (%s) - " CELEPHAIS_DM_BUNDLE_VERSION " (%s)",
                            duid,
                            version);
        rc = celephais_err_request_denied;
        goto exit;
    }

    // write notification
    worker_write_notification(obj, args, celephais_remove_cb);

exit:
    SAH_TRACEZ_INFO(ME, "%s exiting, rc = %d", __func__, rc);
    return rc;
}

celephais_err_type_t celephais_do_garbage_collection(amxd_object_t* obj, amxc_var_t* args) {

    SAH_TRACEZ_INFO(ME, "%s entered", __func__);

    celephais_err_type_t rc = celephais_no_error;
    int res;
    char* bundle_path = NULL;
    const char* bundle_loc = NULL;

    amxd_dm_t* celephais_dm = celephais_get_dm();
    if (celephais_dm == NULL) {
        SAH_TRACEZ_INFO(ME, "Failed to get celephais DM");
        goto exit;
    }
    amxd_object_t* celephais_obj = amxd_dm_get_object(celephais_dm, CELEPHAIS_DM);
    if (celephais_obj == NULL) {
        SAH_TRACEZ_INFO(ME, "Failed to find %s", CELEPHAIS_DM);
        goto exit;
    }

    bundle_loc = amxd_object_get_value(cstring_t, celephais_obj, CELEPHAIS_BUNDLE_LOCATION, NULL);
    if (string_is_empty(bundle_loc)) {
        SAH_TRACEZ_ERROR(ME, "Failed to find %s in celephais data model", CELEPHAIS_BUNDLE_LOCATION);
        goto exit;
    }

    amxd_object_t* bundles = amxd_dm_findf(celephais_dm, CELEPHAIS_DM_BUNDLES);
    if (bundles == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to find %s", CELEPHAIS_DM_BUNDLES);
        goto exit;
    }

    // Iterate through bundles and remove those marked for removal
    amxd_object_for_each(instance, it, bundles) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);

        bool marked_for_removal = amxd_object_get_value(bool, instance, CELEPHAIS_DM_BUNDLE_MARK_RM, NULL);
        if (marked_for_removal) {
            const char* duid = amxd_object_get_value(cstring_t, instance, CELEPHAIS_DM_BUNDLE_DUID, NULL);
            const char* version = amxd_object_get_value(cstring_t, instance, CELEPHAIS_DM_BUNDLE_VERSION, NULL);
            asprintf(&bundle_path, "%s/%s.tar", bundle_loc, duid);
            res = celephais_remove(bundle_path);
            if (res != 0) {
                SAH_TRACEZ_ERROR(ME, "Failed to remove bundle %s:%s during gc, rc (%d)", duid, version, res);
                NOTIF_WORKER_FAILED(obj, NULL, celephais_err_request_denied, CELEPHAIS_CMD_GC, "error executing [delete (%s:%s)], error_code : %d", duid, version, res);
                rc = celephais_err_request_denied;
                continue;
            }
            SAH_TRACEZ_INFO(ME, "Removed bundle %s:%s", duid, version);
        }
    }
    // Notify the main thread to do DM trans and external notifications.
    worker_write_notification(bundles, args, celephais_bundle_removed_cb);

exit:
    free((char*)bundle_loc);
    free(bundle_path);
    return rc;
}
