# Celephais Application

[[_TOC_]]

## Introduction

The Celephais packager application is introduced to handle the download and management of OCI bundles, along with associated adaptor and support libraries, timingila-celephais and libcelephais respectively.
### API

The API is defined in ODL files. Several methods are exposed to manipulate bundle:

* Pull : Download a bundle from a remote repository to the storage directory
* Remove : Set a marker on the specified bundle for the GC
* GC (Garbage Collector) : Remove all bundles that have been marked for removal

### Celephais.pull()  

Celephais.pull() needs the following arguments :

* **URI** : string mandatory
* **UUID** : string mandatory
* **username** : string optional
* **password** : string optional

Celephais uses services provided by libcelephais to pull the OCI bundle.

Username and password are optional.

Sequence diagram of Celephais.pull()  :  

![Celephais pull](celephaispull.png)

### Celephais.remove()  

Celephais.remove() needs the following arguments :

* **paramKey** : string mandatory
* **paramValue** : string mandatory

Celephais.remove() marks bundles in the datamodel for removal. Each bundle with the value *paramValue* in the *paramKey* field is marked.

### Celephais.garbage_collection()  

Celephais.garbage_collection() doesn't need any argument.
It removes any bundles that are marked for removal from the file system and Celephais' datamodel.

### Celephais.signature_verification()  

Signature verification is currently not supported.

### Celephais.update()  

Identical parameters to Celephais.pull() are used

In celephais, bundles are identified via the UUID used to pull them. Because of this a normal pull operation is unsuitable. The tarball associated with the UUID must be overwritten rather than just pulling another bundle.



### Datamodel  

```
> celephais.Bundles.?
celephais.Bundles
celephais.Bundles.1
celephais.Bundles.1.URI=http://192.168.1.182:8000/alpine.tar
celephais.Bundles.1.MarkForRemoval=false
celephais.Bundles.1.Status="Downloaded"
celephais.Bundles.1.Vendor=""
celephais.Bundles.1.Description=""
celephais.Bundles.1.ErrorCode=0
celephais.Bundles.1.UUID=uuid
celephais.Bundles.1.Name="alpine"
celephais.Bundles.1.Version="unknown"

```

The Bundles section has the following parameters :
* **URI**: Remote bundle name used to fetch the bundle file  
    Format : transport://server/bundlefilename  
* **MarkForRemoval**: 
* **Status**: status of bundle
* **Vendor**: Not used
* **Description**: Not used
* **ErrorCode**: Not used
* **UUID**: TR-181 parameter. Should be unique among the bundles.
* **Name**: TR-181 parameter. Should be unique among the bundles.
* **Version**: Not used


## Building and installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    [Get Docker Engine for appropriate platform]( https://docs.docker.com/engine/install/)

    Make sure you user id is added to the docker group:

    ```bash
    sudo usermod -aG docker $USER
    ```

2. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

3. Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites
- [libamxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [libamxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [libamxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libcelephais](https://gitlab.com/prpl-foundation/lcm/libraries/libcelephais)

#### Build and Install

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/applications
    cd ~/workspace/lcm/applications
    git clone https://gitlab.com/prpl-foundation/lcm/applications/celephais
    ```
2. Build it

    ```bash
    cd ~/workspace/lcm/applications/celephais
    mkdir build
    cd build
    cmake ../
    make
   ```

### Installing

#### Using make target install

You can install your own compiled version easily by running the install target.

```bash
cd ~/workspace/lcm/applications/celephais
sudo make install
```
