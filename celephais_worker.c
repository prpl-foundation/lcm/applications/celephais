/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "celephais_worker.h"

#include <stdlib.h>

#include "celephais/celephais_defines.h"

#include <debug/sahtrace.h>

#define ME "celephais_worker"


static celephais_worker_t worker;

static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t mutex_tasks = PTHREAD_MUTEX_INITIALIZER;

celephais_worker_t* celephais_get_worker(void) 
{
    return &worker;
}

void* celephais_worker_loop(UNUSED void* args) 
{
    SAH_TRACEZ_INFO(ME, "Start worker loop");
    celephais_worker_t* w = celephais_get_worker();
    do {
        // Wait a new task to be added in the list
        pthread_mutex_lock(&mutex_tasks);
        while(amxc_lqueue_is_empty(&w->tasks) && w->isRunning) 
        {
            pthread_cond_wait(&cond, &mutex_tasks);
        }

        amxc_lqueue_it_t* t_it = amxc_lqueue_remove(&w->tasks);
        pthread_mutex_unlock(&mutex_tasks);

        while(t_it && w->isRunning) 
        {
            SAH_TRACEZ_INFO(ME, "Execute task on worker thread");
            celephais_worker_task_t* task = amxc_llist_it_get_data(t_it, celephais_worker_task_t, it);
            celephais_err_type_t ret = task->func(task->obj, &task->data);
            if(ret) 
            {
                SAH_TRACEZ_ERROR(ME, "Task returned error  [%d]", ret);
            }
            pthread_mutex_lock(&mutex_tasks);
            t_it = amxc_lqueue_remove(&w->tasks);
            pthread_mutex_unlock(&mutex_tasks);
        }
    } while(w->isRunning);
    return NULL;
}

int celephais_worker_add_task(amxd_object_t* obj, const amxc_var_t* const data, void* func) 
{
    SAH_TRACEZ_INFO(ME, "Add task to worker thread queue");
    int retval = -1;
    celephais_worker_t* w = celephais_get_worker();
    celephais_worker_task_t* task = (celephais_worker_task_t*) calloc(1, sizeof(celephais_worker_task_t));

    amxc_var_init(&task->data);

    if(data != NULL) 
    {
        when_failed(amxc_var_copy(&task->data, data), exit);
    }
    task->obj = obj;
    task->func = func;
    pthread_mutex_lock(&mutex_tasks);

    retval = amxc_lqueue_add(&w->tasks, &task->it);

    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex_tasks);

    retval = 0;

exit:
    if((retval != 0) && (task != NULL)) 
    {
        amxc_var_clean(&task->data);
        free(task);
    }
    return retval;
}

int celephais_init_worker(void) 
{
    int res = 0;
    worker.isRunning = true;
    if((res = amxc_lqueue_init(&worker.tasks)) != 0) 
    {
        SAH_TRACEZ_ERROR(ME, "Failed to initialize tasks queue [Error %d]", res);
        goto exit;
    }
    if((res = pthread_create(&worker.thread, NULL, celephais_worker_loop, NULL)) != 0) 
    {
        SAH_TRACEZ_ERROR(ME, "Failed to create worker thread [Error %d]", res);
        goto exit;
    }
    SAH_TRACE_INFO("Worker started");
exit:
    return res;
}

void celephais_clean_worker(void) 
{
    SAH_TRACEZ_INFO(ME, "Clean worker celephais");
    celephais_worker_t* w = celephais_get_worker();

    w->isRunning = false;
    pthread_mutex_lock(&mutex_tasks);
    pthread_cond_signal(&cond);
    pthread_mutex_unlock(&mutex_tasks);

    pthread_join(w->thread, NULL);

    pthread_cond_destroy(&cond);
    pthread_mutex_destroy(&mutex_tasks);
    amxc_lqueue_clean(&w->tasks, NULL);
    SAH_TRACE_INFO("Worker closed");
}