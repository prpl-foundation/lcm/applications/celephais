# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## Release v1.0.0 - 2024-01-17

### Changes

- Updated DM & param handling for methods & events (For LCM v1.5 compatibility)

### Fixes

- small fixes + properly clean up leftovers when extracting metadata from bundle configs

## Release v0.1.0

### New

- Initial release
