/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "celephais.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_object_expression.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include <debug/sahtrace.h>

#include <celephais/celephais_curl.h>
#include <celephais/celephais_utils.h>
#include "celephais_common.h"
#include "celephais_worker.h"
#include "celephais_worker_func.h"
#include "celephais_worker_signals.h"
#include "celephais_dm.h"

#define ME "celephais_main"


static amxd_dm_t* global_dm = NULL;
static amxo_parser_t* global_parser = NULL;

amxd_dm_t* celephais_get_dm(void) 
{
    return global_dm;
}

amxo_parser_t* celephais_get_parser(void) 
{
    return global_parser;
}

// create the apps storage dirs if they don't exist.
static void init_dirs(void)
{
    amxd_object_t* celephais = amxd_dm_get_object(global_dm, CELEPHAIS_DM);
    char* bundle_location = amxd_object_get_value(cstring_t, celephais, CELEPHAIS_BUNDLE_LOCATION, NULL);

    if(!dir_exists(bundle_location))
    {
        SAH_TRACE_INFO("Creating bundle storage directory");
        if(celephais_create_storage_dir(bundle_location) < 0)
        {
            SAH_TRACEZ_ERROR(ME,"Failed to create storage location!");
        }
    }
    else
    {
        SAH_TRACE_INFO("Storage directory already exists");
    }
}

static void init_dm(void)
{
    amxd_status_t res = amxd_status_unknown_error;
    res = celephais_sync_bundles();
    if(res != amxd_status_ok)
    {
        SAH_TRACEZ_ERROR(ME,"Failed to init DM !");
    }
}

amxd_status_t _celephais_remove(amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret)
{
    amxd_status_t rc = amxd_status_ok;
    amxc_var_t data;
    SAH_TRACE_INFO("Executing %s", __func__);

    amxc_var_init(&data);
    if(args != NULL)
    {
        when_failed(amxc_var_copy(&data, args), exit);
    }
    amxc_var_add_key(cstring_t, &data, CELEPHAIS_NOTIF_COMMAND_ID, GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID));

    int res = celephais_worker_add_task(object, &data, celephais_do_remove);
    if(res != 0)
    {
        rc = amxd_status_unknown_error;
        SET_ERR("Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    SAH_TRACE_INFO("%s exiting with code [%d]", __func__, rc);
    amxc_var_clean(&data);
    return rc;
}

amxd_status_t _celephais_list(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret)
{
    amxd_status_t rc = amxd_status_function_not_implemented; 
    SAH_TRACE_INFO("%s called", __func__ );
    //TODO remove this lib call once this function is being implemented , its only added to show that the library works, and can be linked and called
    //celephais_curl_download_content(NULL,NULL,NULL,NULL,false);
    return rc;
}

amxd_status_t _celephais_status(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret)
{
    amxd_status_t rc = amxd_status_function_not_implemented; 
    SAH_TRACE_INFO("%s called", __func__ );
    return rc;
}

amxd_status_t _celephais_command(amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret)
{

    const char* command = GET_CHAR(args, CELEPHAIS_COMMAND);
    const char* command_id = GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID);

    amxc_var_t* params = GET_ARG(args, CELEPHAIS_COMMAND_PARAMETERS);
    amxc_var_t newargs;
    amxc_var_init(&newargs);
    amxc_var_set_type(&newargs, AMXC_VAR_ID_HTABLE);

    SAH_TRACEZ_INFO(ME, "executing command ('%s' - commandID: [%s])", command, command_id);

    if(params) {
        amxc_htable_for_each(it, amxc_var_constcast(amxc_htable_t, params)) {
            amxc_var_t* item = amxc_var_from_htable_it(it);
            amxc_var_t* value = amxc_var_add_new_key(&newargs, it->key);
            amxc_var_copy(value, item);
            SAH_TRACEZ_INFO(ME, "passing arg [%s=\"%s\"]", it->key, amxc_var_constcast(cstring_t, item));
        }
    }

    if(!string_is_empty(command_id)) {
        amxc_var_add_key(cstring_t, &newargs, CELEPHAIS_NOTIF_COMMAND_ID, command_id);
    }

    const amxd_status_t status = amxd_object_invoke_function(object, command, &newargs, ret);

    amxc_var_clean(&newargs);

    return status;
}

amxd_status_t _celephais_pull(amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret)
{
    amxd_status_t rc = amxd_status_ok;
    amxc_var_t data;
    SAH_TRACE_INFO("Executing _celephais_pull()");

    amxc_var_init(&data);
    if(args != NULL)
    {
        when_failed(amxc_var_copy(&data,args),exit);
    }
    amxc_var_add_key(cstring_t, &data, CELEPHAIS_NOTIF_COMMAND_ID, GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID));

    int res = celephais_worker_add_task(object, &data, celephais_do_pull);
    if(res != 0) 
    {
        rc = amxd_status_unknown_error;
        SET_ERR("Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    SAH_TRACE_INFO("_celephais_pull() exiting with code [%d]", rc);
    amxc_var_clean(&data);
    return rc;
}

amxd_status_t _celephais_update(amxd_object_t* object, UNUSED amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret)
{
    amxd_status_t rc = amxd_status_ok;
    amxc_var_t data;
    SAH_TRACE_INFO("Executing _celehpais_update()");

    amxc_var_init(&data);
    if(args != NULL)
    {
        when_failed(amxc_var_copy(&data,args),exit);
    }
    amxc_var_add_key(cstring_t, &data, CELEPHAIS_NOTIF_COMMAND_ID, GET_CHAR(args, CELEPHAIS_NOTIF_COMMAND_ID));

    int res = celephais_worker_add_task(object, &data, celephais_do_update);
    if(res != 0) 
    {
        rc = amxd_status_unknown_error;
        SET_ERR("Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    SAH_TRACE_INFO("_celephais_pull() exiting with code [%d]", rc);
    amxc_var_clean(&data);
    return rc;
}

amxd_status_t _celephais_garbage_collection(amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, amxc_var_t* ret)
{
    SAH_TRACEZ_INFO(ME, "%s entered", __func__);
    amxd_status_t rc = amxd_status_ok;

    if (celephais_worker_add_task(object, NULL, celephais_do_garbage_collection))
    {
        rc = amxd_status_unknown_error;
        SET_ERR("Couldn't add task to worker's queue");
        goto exit;
    }

exit:
    SAH_TRACEZ_INFO(ME, "%s returned %d", __func__, rc);
    return rc;
}

amxd_status_t _celephais_signature_verification(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret)
{
    SAH_TRACEZ_INFO(ME, "%s entered", __func__);
    amxd_status_t rc = amxd_status_ok;
    // TODO: Extract "Enable" from args and enable/disable signature verification
    SAH_TRACEZ_INFO(ME, "%s returned %d", __func__, rc);
    return rc;
}

int _celephais_main(int command, amxd_dm_t* dm, amxo_parser_t* parser)
{
    int rc = -1;
    
    switch (command) 
    {
        case 0: //start
        {
            SAH_TRACE_INFO("celephais started");
            global_dm = dm;
            global_parser = parser;
            //TODO init data model
            init_dirs();
            init_dm();
            celephais_init_worker();
            worker_sig_ctrl_init();
            rc = 0;
            break;
        }
        case 1: //stop
        {
            SAH_TRACE_INFO("celephais stopped");
            global_dm = NULL;
            global_parser = NULL;
            celephais_clean_worker();
            worker_sig_ctrl_clean();
            rc = 0;
            break;
        }
        default:
        SAH_TRACE_ERROR("command %i is invalid to %s" ,__func__, command);
            break;
    };
    return rc;
}

void _log_event(UNUSED const char* const name,UNUSED const amxc_var_t* const data,UNUSED void* const unused)
{
    SAH_TRACE_INFO("%s called by event %s",__func__,name);
}

